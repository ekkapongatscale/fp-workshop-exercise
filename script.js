document.getElementById('fileinput').addEventListener('change', readFileFromEvent, false)

function readFileFromEvent(event) {
	var file = event.target.files[0]
	var r = new FileReader()
	r.onload = updateContent
	r.readAsBinaryString(file)
}

function logFileLoad(event) {
	var content = event.target.result
	console.log(content)
}

function updateContent(event) {
	// logFileLoad(event)
	setContent(parseMarkDown(event.target.result))
}

function setContent(content) {
	document.getElementById("main").innerHTML = content
}

function parseMarkDown(content) {
	// Do your work here
	let fnSplitContent = (input) => input.split("\n\n")//if not work uncomment.
	//let fnSplitContent = (input) => input.split("\r\n\r\n")//if not work uncomment.

	let fnSplitStrByStr = (str) => (s) => str.split(s)
		.filter(x => x != "")
		.map((input) => { return "<h" + s.length + ">" + input + "</h" + s.length + ">" })

	let fnWrapParagraph = (str) => "<p>" + str + "</p>"

	let fnWrapHeader = (str) => {
		if (str.indexOf("##") >= 0) {
			return fnSplitStrByStr(str)("##")
		} else if (str.indexOf("#") >= 0) {
			return fnSplitStrByStr(str)("#")
		} else {
			return fnWrapParagraph(str)
		}
	}

	return fnSplitContent(content)
		.map((item, index) => fnWrapHeader(item))
		.join("")
}